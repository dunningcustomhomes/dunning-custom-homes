Dunning Custom Homes is a North Carolina custom home builder serving Chapel Hill, Durham, Raleigh, Pittsboro, and surrounding areas. President John Dunning has been building luxury homes for 20 years and has an ironclad reputation for his attention to detail, design skill, and construction quality.

Address: 1297 Chatham Church Rd, Moncure, NC 27559, USA

Phone: 919-923-7298

Website: https://dunningcustomhomes.com
